---
layout: page
title: About
permalink: /about/
---

<!-- This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/) -->

<!-- You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll) -->

<!-- <img src="https://media.licdn.com/dms/image/C4D03AQFENIoLS2coPQ/profile-displayphoto-shrink_200_200/0?e=1556755200&v=beta&t=Vf806MchgN1Okae5iFjcVRt5YNczdKOAivwituXLKgU"/> -->

<div>
    Steve Baker
</div>
<div>
    Full Stack Developer 
</div>
<br/>
<div>
    Experience in:
</div>
<ul>
    <li>
        JavaScript
    </li>
    <li>
        C#
    </li>
    <li>
        Angular
    </li>
    <li>
        SQL
    </li>
    <li>
        Docker
    </li>
</ul>
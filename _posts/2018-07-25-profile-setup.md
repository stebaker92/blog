---
layout: post
permalink: "/profile-setup-automation"
title:  "Automating setting up your local development environment(s)"
date:   2018-07-25 13:21:00 +0100
categories: powershell choco profile
---

# Dotfiles, source controlled development environments & automated profile setup

Setting up a new environment can be such a pain, you need to

- Re-download all your software (if you can remember all the software & tools you had)
- Run 100's of installers (which you can't always run in parallel) & manually click next
- Finally set up your local profile & customize your software (keyboard shortcuts, bash & powershell $profiles, git aliases, IDE settings etc).

Recently I've had to do this 4 times in the past few weeks and every time, automated a little bit more of the process of getting set up - which is important as automation is an crucial part of development & technology

There's a couple of neat advantages to putting the effort in and automating dev environments and it doesn't take long to do:

- A back up of all your settings & config
- History & old versions for any settings & config (deleted a script / alias you thought you'd never use again? Just check the commit history!)
- Pick and choose what files to sync across different machines
- Saves time & effort getting started on a new environment, whether you're changing laptop, jobs or just reinstalling a corrupt version of Windows again!
- Share with and learn from the community!

## Chocolatey package manager

[Choco](https://chocolatey.org/packages/ChocolateyGUI)
is a Windows package manager (similar to apt-get, yum, pacman etc) that allows you to easily download & install software with a simple command: `choco install vscode`. 
By adding all required software to a script (e.g. Install-Choco-Software.ps1), all of your software can be installed within minutes & not requiring any user interaction.

My `Install-Choco-Software.ps1` looks something like this:

```

if (Get-Command choco -errorAction SilentlyContinue)
{
    "Choco is already installed"
}
else 
{
    "Installing Choco..."
    Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# Allow global confirmation, so we don't need to confirm each item we're installing
choco feature enable -n=allowGlobalConfirmation

choco install git
choco install vscode
choco install nvm

```

An (unofficial) alternative to scripting all of your packages out is to use a package called [Choco List Backup](https://chocolatey.org/packages/choco-package-list-backup), which generates a [packages.config](https://github.com/chocolatey/choco/wiki/CommandsInstall#packagesconfig) which you can then import via `choco install packages.config`

## Profile & Dotfiles
Once all of your software is installed, it's always good to customize it to tailor to your needs - 
for example adding your [aliases and settings to .gitconfig](https://githowto.com/aliases) (`git undo` is one of my favourites for when you royally screw that last commit up: `undo = reset HEAD~1 --mixed`) , settings & alises in PowerShell / bash & changing your vscode settings and extensions, etc.

[Setting up your PowerShell profile](https://www.howtogeek.com/50236/customizing-your-powershell-profile/)
 is easy and something that you should add too over time as you identify tasks you can automate through aliases or just to customize your shell settings, whether it's adding directory's to your path if you don't want to clutter up your global $PATH or if you want to set font colours & size for your PowerShell console

These files are commonly called Dotfiles and allow you to fully customize your environment to tailor to your needs, from something small to changing a termials default font colour to some funky time saving aliases

Backing up your profile (and dot files) up to source control makes it easy to setup any software by just cloning a repo and copying them to the correct folders (.e.g C:\users\YOU\.gitconfig), or setting up symlinks (see the next section)

It's also easy to pick and choose what configs you want to use (or not):

- setup-home-profile.ps1
- setup-work-profile.ps1
- .gitconfig
- Microsoft.PowerShell_profile.ps1
- Home/
  - vscode-settings.json
- Work/
  - vscode-settings.json
  - conemu.xml
  - ...

##  Settings symlinks
A symlink (symbotic link) is a special kind of file that points to another file, similar to a shortcut. 

Using symlinks for your profile means you can edit your profile in your git folder and create a symlink anywhere on your harddrive which points to this file, meaning you don't have to worry about copying & pasting files about to back them up in source control or files getting out of sync, whatever content is in your git folder is referenced too by the symlink in your profile directory! 

Symlinks are easy to setup, in PowerShell the following will setup a symlink:
`New-Item -Path $destFile -ItemType SymbolicLink -Value $sourceFile -Force`

## PowerShell modules
[PowerShell modules](http://www.tomsitpro.com/articles/powershell-modules,2-846.html) 
are a good way to split out functionality into modules that can be split out from your profile, imported and shared; helping maintain your PowerShell profile
(and any other random scripts you may have) such as a Module for manipulating files.

If you had a lot of files / aliases for modifying values in an XML & Json files, this could all be refactored into a `Data-Manipulation.psm1` PowerShell module and referenced from your profile and any other files that need the Functions: `Import-Module Data-Manipulation.psm1` )

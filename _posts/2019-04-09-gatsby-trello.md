---
layout: post
permalink: "/gatsby-trello-portfolio"
title:  "Creating a portfolio site using Gatsby & Trello"
date:   2019-04-09 11:22:33 +0100
categories: gatsby portfolio tutorial trello ssg
---

## What is Gatsby? 

> Gatsby is a free and open source framework based on React that helps developers build blazing fast websites and apps
- [https://www.gatsbyjs.org/](https://www.gatsbyjs.org/)

Basically, Gatsby allows you to build a static site, similar to Jekyll but with a lot more to offer as you can use a range of node packages & use React features such as Components etc. You will build a site using React but when built, a static site will be generated (with minimal JavaScript & dependencies - just good ol' HTML and CSS!)

## My Trello 'Projects' board

The idea for this project was to expose _some_ of the data on my personal Trello board & try to expose some of the interesting projects I've done in the past or am currently working on. The information was already in this board so why not expose it (as opposed to duplicating this data in a Content Management System)

## Hooking it up to Trello

This project was surprisingly simple in the end, I basically ended up with a handful of components - a ProjectList Component, ProjectItem Component and ProjectDetail Component ('Project' mapping to a single Trello Card).

To scrape the data from Trello & dynamically generate a page per Trello Card (& also a Project List page), I used the [gatsby node APIs](https://www.gatsbyjs.org/docs/node-apis/) and ended up with something like this:

```

...
// Use a Trello API to gather the data
const trelloCards = myTrelloHelper.getAllCards();
...

exports.createPages = ({ boundActionCreators }) => {
  const { createPage } = boundActionCreators;

  // Page to list all projects
  createPage({
    path: `/`,
    component: require.resolve("./src/all-projects-template.js"),
    // Data for this page - we require all of the Trello Cards & Labels
    context: { trelloCards },
  })

  const cardTemplate = path.resolve(`src/card-template.js`);

  var showdown = require('showdown');
  var converter = new showdown.Converter();

  // Create a page for each Trello Card
  cards.forEach((card) => {
    const path = card.path;

    // Convert our Trello markdown to HTML
    card.details = converter.makeHtml(page.desc);

    createPage({
      path,
      component: cardTemplate,

      // Send the card data to this component so we can render the cards details
      context: {
        card
      }
    });
  });
};

```

## Benefits & Drawbacks

For this project, some of the benefits of using a static site & using Trello were: 
- Security - The data source (Trello) isn't publicly available - only GitLab and my local Dev environment need an API Key (although it's just a readonly key, I still wouldn't want users to access my other Trello boards or cards!)
- Cost - Hosting a static site is very cheap / free as there is very little architecture - no API's to host & using Trello means no cost for data storage
- Stability - As this is a static site with no JavaScript, no API's and no database, there's less points of failure! No worries of API's going down or databases having issues etc. If the API being consumed does have performance issues or is down, the end user won't see this - the generated content will just be slightly outdated. 
- Speed - No API's & static content means there's very little / no JavaScript (bulky front end frameworks will drastically reduce page load and render speeds, especially when not configured properly or minified!) & everything can be cached via a Content Delivery Network. 
- Effort - I don't have to manage my portfolio! - as long as I add the correct tags & readable content on my private Trello board, it will get transformed into a readable web page. No managing 
- Minimal learning curve - I've done some (basic) React projects in the past, making it easy to get started with Gatsby!

The main drawback I found was: 
- It currently doesn't update in real time (although I have my project set to rebuild & redeploy on a regular schedule or I could use Trello web-hooks to trigger a build which seemed a little overkill)


## Conclusion

As you can imagine, this approach is quite flexible - to hook this up to a different data source (a CMS, any API or even a different Trello board!) would only require minimal changes

If you want to see the end result, come and have a look and feel free to drop me any questions or feedback you may have!

[https://stebaker92.gitlab.io/portfolio/](https://stebaker92.gitlab.io/portfolio/)